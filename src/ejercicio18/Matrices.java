package ejercicio18;

public class Matrices {

	static int [][] cuadrada1(int dim) {
		int [][] matriz = new int[dim][dim];
		int contador = 0;
		for (int y = 0; y < matriz.length; y++) {
			for (int x = 0; x < matriz.length; x++) {
				matriz[x][y] = ++contador;
			}
		}
		return matriz;
	}

	static int [][] cuadrada2(int dim) {
		int [][] matriz = new int[dim][dim];
		int contador = 0;
		int y = 0;
		int x = 0;
		
		while (y < dim) {
			if ((y % 2 == 0)) {
				x = 0;
				while (x < dim) {
					matriz[x][y] = ++contador;
					x++;
				}
			} else {
				x=(dim-1);
				while (x >= 0) {
					matriz[x][y] = ++contador;
					x--;
				}
			}
			y++;
		}
		return matriz;
	}
	
	static String [][] palindromos(int c, int f) {
		String[] abecedario = {
				"A","B","C","D","E","F","G","H","I","J","K","L","M"
		};
		
		String letras1y3;
		String letra2;
		
		String [][] matriz = new String[f][c];
		
		for (int y = 0; y < matriz[0].length; y++) {
			System.out.println("Columna " + y);
			for (int x = 0; x < matriz.length; x++) {
				letras1y3 = abecedario[x];
				letra2 = abecedario[(x+y)];
				matriz[x][y] = letras1y3 + letra2 + letras1y3;
				
				System.out.println("Fila " + x);
			}
		}
		System.out.println("FIN");
		return matriz;
	}
	
	
}