package ejercicio18;

public class PruebaMatrices {
	
	public static void imprimirMatriz( int [][] matriz) {
		System.out.println();
		for (int x = 0; x < matriz.length; x++) {
			for (int y = 0; y < matriz.length; y++) {
				System.out.print(matriz[x][y] +  " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public static void imprimirMatriz( String [][] matriz) {
		System.out.println();
		for (int x = 0; x < matriz.length; x++) {
			for (int y = 0; y < matriz[0].length; y++) {
				System.out.print(matriz[x][y] +  " ");
			}
			System.out.println();
		}
		System.out.println();
	}

	public static void main(String[] args) {
		//System.out.println("Cuadrada1");
		//imprimirMatriz(Matrices.cuadrada1(4));
		
		//System.out.println("Cuadrada2");
		//imprimirMatriz(Matrices.cuadrada2(5));
		
		System.out.println("Palindromos");
		imprimirMatriz(Matrices.palindromos(6,4));

	}

}
