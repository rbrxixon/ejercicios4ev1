package ejercicios4ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
		String entrada1,entrada2 = null;
		int apariciones = 0;
		
		try {
			System.out.println("Escriba una cadena para compararlas el numero de veces que la segunda esta contenida en la primera");
			
			System.out.println("Cadena1");
			entrada1 = reader.readLine();
		
			System.out.println("Cadena2");
			entrada2 = reader.readLine();
			
		   while (entrada1.indexOf(entrada2) > -1) {
			   entrada1 = entrada1.substring(entrada1.indexOf(entrada2) + entrada2.length(), entrada1.length());
   			   apariciones++;
		    }
		   System.out.println("Apariciones: " + apariciones);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
