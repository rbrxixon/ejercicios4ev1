package ejercicios4ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Random;

public class Ejercicio7 {
	static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
    static String entrada = null;
    static int opcion, diferencia;
    Random r;
    static int vector [];
	static int numeroGenerado;
	static boolean coincidencia;
	static long timeElapsed,tiempoVector,tiempoDiferenciaMenor;
	static Instant start, finish;
    
	public static void mostrarMenu() {
		try {
			System.out.println("Especifica tamaño vector entre 10 y 1.000.000");
			entrada = reader.readLine();
			opcion = Integer.parseInt(entrada);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void buscarNumero (int i) {
		coincidencia = false;
		for (int j = 0; j < i; j++) {
			if (vector[j] == numeroGenerado) {
				//System.out.println("Coincide, buscar otro. vector[" + i + "] " + vector[j] + " numeroGenerado " + numeroGenerado);
				coincidencia = true;
				break;
			}
		}
			
		if (coincidencia) {
			generarNumero();
			buscarNumero(i);
		} else {
			//System.out.println("Almacenar en [" + i + "] : " + numeroGenerado);
			vector[i] = numeroGenerado;
		}
	}
	
	public static void empezarCronometro () {
		start = Instant.now();
	}
	
	public static long pararCronometro() {
		finish = Instant.now();
		timeElapsed = Duration.between(start, finish).toMillis();
		return timeElapsed;
	}
	
	public static void crearVector() {
		vector = new int[opcion];
	}
	
	public static void llenarVector() {
		empezarCronometro();
		for (int i = 0; i < vector.length; i++) {
			generarNumero();
			buscarNumero(i);
		}
		tiempoVector = pararCronometro();
	}
	
	public static int generarNumero () {
		numeroGenerado = new Random().nextInt(1999999)-(1000000);
		//System.out.println("NumeroGenerado " + numeroGenerado);
		return numeroGenerado;
	}
	
	public static void diferenciaNumeros() {
		empezarCronometro();
		Arrays.sort(vector);
		int menor, mayor;
		menor = vector[0];
		mayor = vector[vector.length-1];
		diferencia = (mayor-menor);
		//System.out.println("Menor: " + menor + " Mayor: " + mayor + " Diferencia: " + diferencia);
		tiempoDiferenciaMenor = pararCronometro();
	}
	
	public static void mostrarInformacion() {
		System.out.println("Tamaño vector: " + vector.length);
		System.out.println("Tiempo empleado en llenarlo: " + tiempoVector + " milisegundos");
		System.out.println("Diferencia entre el menor y el mayor de los numeros almacenados en el vector: " + diferencia);
		System.out.println("Tiempo empleado en calcular la diferencia entre el menor y el mayor de los numeros almacenados: " + tiempoDiferenciaMenor);
	}
	
	public static void main(String[] args) {

		do {
			mostrarMenu();
			crearVector();
			llenarVector();
			diferenciaNumeros();
			mostrarInformacion();
		} while (opcion < 10 || opcion > 1000000);
	}
}
