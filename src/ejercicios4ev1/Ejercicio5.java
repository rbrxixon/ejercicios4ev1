package ejercicios4ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio5 {

	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
	    String entrada;
	    char letra;
	    int dni,restoLetraCalculada;
	    char[] arrayLetras = {
	    		'T',
	    		'R',
	    		'W',
	    		'A',
	    		'G',
	    		'M',
	    		'Y',
	    		'F',
	    		'P',
	    		'D',
	    		'X',
	    		'B',
	    		'N',
	    		'J',
	    		'Z',
	    		'S',
	    		'Q',
	    		'V',
	    		'H',
	    		'L',
	    		'C',
	    		'K',
	    		'E'
	    };
	    
	    
		try {
			
			System.out.println("Escribe NIF completo (numero y letra)");
			entrada = reader.readLine();
			
			dni = Integer.parseInt(entrada.substring(0, entrada.length()-1));
			System.out.println("dni " + dni);
			restoLetraCalculada = (dni % 23);
			
			System.out.println("restoLetraCalculada " + restoLetraCalculada);
			
			System.out.println("");
			letra = entrada.substring(entrada.length()-1,entrada.length()).charAt(0);
			System.out.println("Letra " + letra);
			
			if (arrayLetras[restoLetraCalculada] == letra) {
				System.out.println("DNI Correcto");
			} else {
				System.out.println("DNI Incorrecto");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
