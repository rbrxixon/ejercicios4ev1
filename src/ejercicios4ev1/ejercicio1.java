package ejercicios4ev1;

import java.util.Scanner;

public class ejercicio1 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String entrada;
		
		
		char[] vocales = { 'a','e','i','o','u' };
		int [] repeticiones = new int [5];
		
		System.out.println("Esciba una cadena para ver cuantas veces se repite cada vocal");
		entrada = in.next();
		System.out.println(entrada);
		
		char[] charArray = entrada.toCharArray();
		
		for (int x = 0; x < charArray.length; x++) {
			for (int y = 0; y < vocales.length; y++) {
				if (charArray[x] == vocales[y]) {
					repeticiones[y]++;
				}
			}
		}					
		
		for (int i = 0; i < repeticiones.length; i++) {
			System.out.println(vocales[i] + " : " + repeticiones[i]);
		}

			
	}
		
}

