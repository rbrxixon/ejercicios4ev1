package ejercicios4ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Ejercicio4 {

	public static void main(String[] args) {
	    int numTiradas, tirada;
	    int[] contadorCaras = null;
	    
	    try {
	    	System.out.print("Cuantas veces quieres tirar el dado?: ");
	    	numTiradas = Integer.parseInt(new BufferedReader(new InputStreamReader(System.in)).readLine());
	    	contadorCaras = new int[7];
	    	
			for (int i = 0; i < numTiradas; i++) {
				tirada = new Random().nextInt(6)+1;
				contadorCaras[tirada]++;
				System.out.println("Ha salido el " + tirada);
			}
			
			for (int j = 1; j < contadorCaras.length; j++) {
				System.out.println("Numero " + j + " ha salido " + contadorCaras[j] + " veces");	
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
