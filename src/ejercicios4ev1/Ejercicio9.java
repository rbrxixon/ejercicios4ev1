package ejercicios4ev1;

import java.util.ArrayList;
import java.util.Random;

public class Ejercicio9 {
	static int numeroGenerado;
    static int vector [];
    static ArrayList<Integer> numeros;
    static ArrayList<Integer> numeros2;
    static int coincidencias;
    static int index2 = 0;
	static int indice = 0;
	static boolean coincide = false;
    
	public static int generarNumero () {
		numeroGenerado = new Random().nextInt(200)-(100);
		//System.out.println("NumeroGenerado " + numeroGenerado);
		return numeroGenerado;
	}
	
	public static void crearVector() {
		vector = new int[new Random().nextInt(500)+10];
		System.out.println("Tamano Vector " + vector.length);
		numeros2 = new ArrayList<Integer>();
		numeros = new ArrayList<Integer>();
	}
	
	public static void llenarVector() {
		for (int i = 0; i < vector.length; i++) {
			vector[i] = generarNumero();
		}
	}
	
	public static void mostrarVector() {
		System.out.println("Contenido Vector");
		for (int elemento : vector) {
			System.out.println(elemento);
		}
	}
	
	public static void secuenciasNumerosRepetidos() {
		for (int i = 0; i < vector.length; i++) {
			if ((vector.length-1) == i) {
				//System.out.println("Se va acabar el array desde secuenciasNumerosRepetidos");
				break;
			} else {
				//System.out.println("buscarNumero desde secuenciasNumerosRepetidos");
				buscarNumero(vector[i], i);
				i = (index2 -1);
			}
		}
	}
	
	public static void buscarNumero (int numero, int indice) {
		
		boolean primerNumero = true;
		index2 = indice;
		
		do {
			//System.out.println("Do->While");
			if ((vector.length-1 > index2)) {
				//System.out.println("Aumento index2");
				index2++;
			} else {
				//System.out.println("Se acabo el array desde buscarNumero");
				break;
			}
			
			//System.out.println("Comparando -> vector[" + index2 + "]: " + vector[index2] + " | Numero: " + numero);
			if (vector[index2] == numero) {
				coincide = true;
				//System.out.println("Coindice = TRUE");
				if (primerNumero) {
					//System.out.println("Insertando primer numero: " + vector[indice]);
					numeros2.add(vector[indice]);
					primerNumero = false;
					coincidencias++;
				}
				numeros2.add(vector[index2]);
			} else {
				//System.out.println("Coindice = false");
				coincide = false;
			}
			//System.out.println("Index " + indice + " | Index2: " + index2 + " | Leng Vector-1 " + (vector.length-1) + " | Leng Vector " + vector.length + " | coincide="+ coincide);
		} while (coincide && (vector.length > index2));
		
	}

	
	public static void mostrarSecuenciaNumerosRepetidos() {
		System.out.println("Mostrar Secuencia Numeros Repetidos");
		
		for (int i = 0; i < numeros2.size(); i++) {
			System.out.println(numeros2.get(i));
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		crearVector();
		llenarVector();
		if (vector.length < 50) {
			mostrarVector();
		}
		secuenciasNumerosRepetidos();
		mostrarSecuenciaNumerosRepetidos();
		System.out.println("Secuencias: " + coincidencias);
	}
	
	

}

