package ejercicios4ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class Ejercicio8 {
	static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
    static String entrada = null;
    static int opcion,suma,resta;
    static int vector [];
    static ArrayList<Integer> sumaA, restaA, auxA;
	static int numeroGenerado;
    
	public static void mostrarMenu() {
		try {
			System.out.println("Especifica tamaño vector entre 10 y 200");
			entrada = reader.readLine();
			opcion = Integer.parseInt(entrada);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static int generarNumero () {
		numeroGenerado = new Random().nextInt(200)-(100);
		//System.out.println("NumeroGenerado " + numeroGenerado);
		return numeroGenerado;
	}
	
	public static void llenarVector() {
		for (int i = 0; i < vector.length; i++) {
			vector[i] = generarNumero();
		}
	}
	
	public static void crearVector() {
		vector = new int[opcion];
	}
	
	public static void mostrarInformacion() {
		
		System.out.println("Contenido Vector");
		for (int elemento : vector) {
			System.out.println(elemento);
		}
		
		System.out.println("Cantidad numeros que se han sumado");
		for (int elemento : sumaA) {
			System.out.println(elemento);
			suma+=elemento;
		}
		System.out.println("Total Suma: " + suma);
		
		System.out.println("Cantidad numeros que no se han sumado");
		for (int elemento : restaA) {
			System.out.println(elemento);
			resta+=elemento;
		}
		System.out.println("Total Resta: " + resta);
		
	}
	
	public static void calcularVector() {
		sumaA = new ArrayList<Integer>();
		restaA = new ArrayList<Integer>();
		auxA = new ArrayList<Integer>();
		int aux = 0;
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] == 13) {
				if ((vector.length - i) >= 13) {
					restaA.add(vector[i]);
				} else if ((vector.length < 13)) {
					for (int j = i; j < vector.length; j++) {
						aux += vector[j];
						auxA.add(vector[j]);
					}
					if (aux != 7) {
						restaA.addAll(auxA);
					} else {
						sumaA.addAll(auxA);
					}
				}
			} else {
				sumaA.add(vector[i]);
			}
		}
	}
	
	public static void main(String[] args) {
		do {
			mostrarMenu();
			crearVector();
			llenarVector();
			calcularVector();
			mostrarInformacion();
		} while (opcion < 10 || opcion > 200);
	}
}
