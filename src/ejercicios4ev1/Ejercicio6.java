package ejercicios4ev1;

import java.util.Random;

public class Ejercicio6 {
	static int[] numerosEnteros;
	static int[] numerosEnterosCopia;
	static int numeroGenerado;
	static boolean coincidencia;
	
	
	public static void buscarNumero (int i) {
		coincidencia = false;
		for (int j = 0; j < i; j++) {
			if (numerosEnteros[j] == numeroGenerado) {
				//System.out.println("Coincide, buscar otro. numerosEnteros[" + i + "] " + numerosEnteros[j] + " numeroGenerado " + numeroGenerado);
				coincidencia = true;
				break;
			}
		}
			
		if (coincidencia) {
			generarNumero();
			buscarNumero(i);
		} else {
			//System.out.println("Almacenar en [" + i + "]");
			numerosEnteros[i] = numeroGenerado;
		}
		
	}
	
	public static int generarNumero () {
		numeroGenerado = new Random().nextInt(200)-(100);
		//System.out.println("NumeroGenerado " + numeroGenerado);
		return numeroGenerado;
	}
	
	public static void generar2vector() {
		numerosEnterosCopia = new int[numerosEnteros.length];
		int y = 0;
		for (int x = numerosEnteros.length-1 ; x >= 0; x--) {
			numerosEnterosCopia[y] = numerosEnteros[x];
			//System.out.println("Copia de numerosEnterosCopia[" + y + "] " + numerosEnterosCopia[y] + " numerosEnteros[" + x + "] " + numerosEnteros[x]);
			y++;
		}
	}
	
	public static void mostarVectores() {
		System.out.println("Vector 1");
		for (int i = 0; i < numerosEnteros.length; i++) {
			System.out.println(numerosEnteros[i]);
		}
		
		System.out.println("Vector 2");
		for (int i = 0; i < numerosEnterosCopia.length; i++) {
			System.out.println(numerosEnterosCopia[i]);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		numerosEnteros = new int[new Random().nextInt((50-10) + 1)];
		//System.out.println("** Numeros a almacenar: " + numerosEnteros.length);
		for (int i = 0; i < numerosEnteros.length; i++) {
			generarNumero();
			buscarNumero(i);
		}
		generar2vector();
		mostarVectores();
	}
	

}
