package juegoBarcos;

public class Jugador {
	String nombre;
	int numeroIntentos;
	
	public Jugador(String nombre) {
		this.nombre = nombre;
		this.numeroIntentos = 0;
	}

	public String getNombre() {
		return nombre;
	}

	public int getNumeroIntentos() {
		return numeroIntentos;
	}

	public void setNumeroIntentos(int numeroIntentos) {
		this.numeroIntentos = numeroIntentos;
	}

	@Override
	public String toString() {
		return "Jugador [nombre=" + nombre + ", numeroIntentos=" + numeroIntentos + "]";
	}
	
	
	

}
