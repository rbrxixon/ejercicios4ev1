package juegoBarcos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class Juego {
	Tablero tablero;
	ArrayList<Barco> barcos;
	ArrayList<Jugador> jugadores;
	int tamanioTableroX = 10;
	int tamanioTableroY = 10;
	boolean seguir = true;
	
	public int generarNumero (int max) {
		int numeroGenerado = new Random().nextInt(max);
		//System.out.println("NumeroGenerado " + numeroGenerado);
		return numeroGenerado;
	}
	
	public void crearBarcos(Jugador jugador, int cantidad) {
		for (int i = 0; i < cantidad; i++) {
			barcos.add(new Barco(jugador));
		}
	}
	
	public void posicionarBarcosInicial() {
		for (int i = 0; i < barcos.size(); i++) {
			barcos.get(i).setX(generarNumero(tamanioTableroX));
			barcos.get(i).setY(generarNumero(tamanioTableroY));
			System.out.println(barcos.get(i).toString());
		}
	}
	
	public boolean comprobarPosicion(int x, int y, Barco barco) {
		if ((barco.getX() == x) && (barco.getY() == y)) {
			return true;
		}
		return false;
	}
	
	public void preguntarDonde() {
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
	    String entrada;
	    int entradaX, entradaY;
	    int intentos = 0;
		try {
			System.out.println("Indica posicion donde crees que esta el barco rival");
			System.out.println("Posicion X");
			entrada = reader.readLine();
			entradaX = Integer.parseInt(entrada);
			
			System.out.println("Posicion Y");
			entrada = reader.readLine();
			entradaY = Integer.parseInt(entrada);
			
			if (comprobarPosicion(entradaX,entradaY,barcos.get(1))) {
				System.out.println("HUNDIDO!");
				System.out.println("Intentos: " + jugadores.get(1).getNumeroIntentos());
				seguir = false;
			} else {
				System.out.println("AGUA");
				intentos = jugadores.get(1).getNumeroIntentos();
				jugadores.get(1).setNumeroIntentos(++intentos);
				System.out.println("Intentos: " + intentos);
				
			}
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public static void main(String[] args) {

		Juego juego = new Juego();
		juego.barcos = new ArrayList<Barco>();
		juego.jugadores = new ArrayList<Jugador>();
		juego.jugadores.add(new Jugador("Humano"));
		juego.jugadores.add(new Jugador("Maquina"));
		juego.crearBarcos(juego.jugadores.get(0),1);
		juego.crearBarcos(juego.jugadores.get(1),1);

		
		
		juego.tablero = new Tablero(juego.tamanioTableroX,juego.tamanioTableroY);
		juego.posicionarBarcosInicial();
		
		while (juego.seguir) {
			juego.preguntarDonde();
		}
		

	}

}
